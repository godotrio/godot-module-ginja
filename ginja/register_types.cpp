
#include "core/class_db.h"
#include "register_types.h"
#include "ginja.h"

void register_ginja_types()
{
    ClassDB::register_class<Ginja>();
}

void unregister_ginja_types()
{
    // nothing is cool
}
