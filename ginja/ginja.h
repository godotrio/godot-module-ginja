#ifndef GINJA_H
#define GINJA_H

#include <string>

#include <locale>
#include <codecvt>
#include <vector>
#include <cstring>
#include <cwchar>

#include "core/reference.h"
#include "core/io/json.h"

#include "nlohmann/json.hpp"
#include "inja/inja.hpp"
#include "inja/string_view.hpp"


// Just for convenience
using injason = nlohmann::json;


class Ginja : public Reference {
    GDCLASS(Ginja, Reference);

    int count;

protected:
    std::string gs2s(const String &gstr);
    std::string ws2s(const std::wstring &wstr);

    static void _bind_methods();

public:
    String render(const String &gs_template, const Dictionary &variables);

    Ginja();
};

#endif
