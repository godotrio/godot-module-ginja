#include "ginja.h"


std::string Ginja::ws2s(const std::wstring &wstr)
{
    using convert_type = std::codecvt_utf8<wchar_t>;
    std::wstring_convert<convert_type, wchar_t> converter;

    return converter.to_bytes(wstr);
}


std::string Ginja::gs2s(const String &gstr)
{
    return Ginja::ws2s(gstr.ptr());
}


String Ginja::render(const String &gs_template, const Dictionary &variables)
{
    const std::string s_template = Ginja::gs2s(gs_template);

    // To pivot our Dictionary of Variants to a nlohmann::json object,
    // we're using serialization and then deserialization. It's cheap.
    // There may be drawbacks, though. Bigger memory footprint, etc.
    // This has not been extensively tested. Contributions are welcome :3

    // First we serialize our Dictionary using Godot's JSON.
    const String gs_variables = JSON::print(variables);

    // print_line(j_variables);

    // Then we deserialize using nlohmann::json (injason)
    injason data = injason::parse(Ginja::gs2s(gs_variables));

    // Now we have everything we need to ask Inja to render
    std::string ss_return = inja::render(s_template, data);

    //fprintf(stdout, "[ginja] Rendered:\n%s\n", ss_return.c_str());

    return String::utf8(ss_return.c_str());
}


void Ginja::_bind_methods()
{
    ClassDB::bind_method(D_METHOD("render", "template", "variables"), &Ginja::render);
}


Ginja::Ginja() {}
