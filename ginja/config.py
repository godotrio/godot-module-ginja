

def can_build(env, platform):
    # /!. Not all platforms and env have been tested yet.
    return True


def configure(env):
    pass


def get_doc_classes():
    return [
        "Ginja",
    ]


def get_doc_path():
    return "doc_classes"
