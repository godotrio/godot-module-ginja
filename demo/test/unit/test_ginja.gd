extends "res://addons/gut/test.gd"

# LIFECYCLE HOOKS ##############################################################

#func before_each():
#	gut.p("ran setup", 2)

#func after_each():
#	gut.p("ran teardown", 2)

#func before_all():
#	gut.p("ran run setup", 2)

#func after_all():
#	gut.p("ran run teardown", 2)

################################################################################

func test_instantiating():
	var engine = Ginja.new() # https://framagit.org/godotrio/godot-module-ginja
	assert_is(engine, Ginja, "You probably do not have the ginja module.")

# VARIABLES ####################################################################

func test_empty_variables_dictionary():
	var engine = Ginja.new()
	assert_eq(engine.render("Hello!", {}), "Hello!")

# Not sure what to do with this.
#func test_null_variables_dictionary():
#	var engine = Ginja.new()
#	assert_eq(engine.render("Hello!", null), "Hello!")

func test_variable_string():
	var engine = Ginja.new()
	var variables = { 'name': 'Godot' }
	var template = "Hello {{ name }}!"
	assert_eq(
		engine.render(template, variables),
		"Hello Godot!"
	)

func test_variable_integer():
	var engine = Ginja.new()
	var variables = { 'hype': 3 }
	var template = "{{ hype }} hellos Godot{% for i in range(hype) %}!{% endfor %}"
	assert_eq(
		engine.render(template, variables),
		"3 hellos Godot!!!"
	)

func test_variable_float_transcendental():
	var engine = Ginja.new()
	var variables = { 'circle': TAU }
	var template = "The perimeter of a circle is {{ circle }} times its radius"
	assert_eq(
		engine.render(template, variables),
		"The perimeter of a circle is 6.283185 times its radius"
	)

func test_variable_float_rational():
	var engine = Ginja.new()
	var variables = { 'float': 1.0/3.0 }
	var template = "One third ≅ {{ float }}"
	assert_eq(
		engine.render(template, variables),
		"One third ≅ 0.333333"
	)

func test_variable_float_decimal():
	var engine = Ginja.new()
	var variables = { 'float': 4.2 }
	var template = "{{ float }}"
	assert_eq(
		engine.render(template, variables),
		"4.2"
	)

func test_variable_array():
	var engine = Ginja.new()
	var variables = { 'suite': [ 0, 0.618, 'oida' ] }
	var template = "{% for value in suite %}{% if not loop.index == 0 %} {% endif %}{{ value }}{% endfor %}"
	assert_eq(
		engine.render(template, variables),
		"0 0.618 oida"
	)

func test_variable_dictionary():
	var engine = Ginja.new()
	var variables = { 'garnet': { 'ruby': 'red', 'sapphire': 'blue' } }
	var template = "{{ garnet.ruby }}"
	assert_eq(
		engine.render(template, variables),
		"red"
	)

