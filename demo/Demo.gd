extends Node2D

func _ready():

	print("Running the demo…")
	
	var ginja = Ginja.new()
	
	var template = \
"""
Hellö, {{ name }} {% for n in sort(names) %}and {{ n }} {% endfor %}{% for i in range(enthusiasm) %}!{% endfor %}
"""
	var variables = {
		'name': "World⋅s",
		'names': ["A", "C", "B"],
		'enthusiasm': 3,
	}
	
	var expected = \
"""
Hellö, World⋅s and A and B and C !!!
"""
	var actual = ginja.render(template, variables)
	
	assert expected == actual
