Bridge [Inja](https://github.com/pantor/inja) into [Godot](https://github.com/godotengine/godot).

A template engine such as _inja_ is very convenient for:
- shader generation
- dynamic roleplay


# Usage

``` python
var ginja = Ginja.new()
var template = "Hello {{ name }} {% for i in range(enthusiasm) %}!{% endfor %}"
var variables = {'name': "Godot", 'enthusiasm': 3}

print(ginja.render(template, variables)) # "Hello Godot !!!"
```

Inja has many features for more complex uses. See [the doc](https://github.com/pantor/inja#tutorial).


# Demo

The demo project in `demo` is basically the code above as a crude test-suite.
There's also a full example of shader generation for a gooey mouse cursor.

![Goo Cursor Demo 00](demo/showcase_00.gif) ![Goo Cursor Demo 01](demo/showcase_01.gif)


# Installation

- [Install inja](https://github.com/pantor/inja#integration) _(using cget works well)_.
- Clone Godot.
- Copy the module's `ginja` directory into Godot's `modules` directory.
- [Compile Godot](https://docs.godotengine.org/en/3.0/development/compiling/index.html).


# How it works

```mermaid
graph TB
    subgraph Third-Party
    Inja((Inja))
    end
    
    subgraph Ginja Module
    GinjaIn((Input Driver))
    GinjaOut((Output Driver))
    end
    
    subgraph GdScript
    Template[Template]
    Variables[Variables]
    Out[Goodies]
    end
    
    Template -->|String| GinjaIn
    Variables -->|Dictionary| GinjaIn
    
    GinjaIn -->|nlohmann::json| Inja
    Inja -->|std::string| GinjaOut
    GinjaOut -->|String| Out
```


# Compatibility

Tested under Godot 3.1 on linux. Please report what works for you.


# Future

- Discuss/Try out Jinja2Cpp as well
- If you feel like writing a template engine in pure gdscript, do ping me!
